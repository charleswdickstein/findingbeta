from flask import Flask
from flask_restplus import Resource, Api
from flask import render_template
import pandas as pd
import matplotlib.pyplot as plt
import datetime as dt
import pandas_datareader.data as web
from sklearn import linear_model
from sklearn.linear_model import LinearRegression

start = dt.datetime(2017,9,1)
end = dt.datetime(2020,9,1)

app = Flask(__name__)
api = Api(app)

@app.route("/<string:ticker>")
def index(ticker):
    beta = get_beta(ticker,start,end)

    img_file_name = ticker+".png"
    return render_template("beta_plot.html",beta_val=beta,imgfile=img_file_name)

@api.route('/<string:ticker>')
@api.param('ticker', 'Stock ticker')
class Beta(Resource):
    def get(self, ticker):
        #salary = model.predict([[years_exp]])
        #return {'years experience received': years_exp,
        #        'expected salary': salary[0].round(2)}

        
        
        #return {'beta':beta}

        return render_template("index.html")

def get_stock_data(ticker, start, end):
    stock_data = web.DataReader(ticker, 'yahoo', start, end)
    stock_data = stock_data.rename(columns={"Close":ticker+"_Close"})
    return stock_data

def preprocess_data(spy_df, stock_df, ticker):
    df = spy_df.merge(stock_df, how="inner", on="Date")
    df = df[["SPY_Close", ticker+"_Close"]]
    df = df.reset_index()
    df["Date"] = pd.to_datetime(df['Date']).sort_values()
    df.set_index('Date',inplace=True)
    return df

def get_regression_and_plot(df, ticker):
    X = df[["SPY_Close"]].pct_change()[1:]
    y = df[[ticker+"_Close"]].pct_change()[1:]
    regr = linear_model.LinearRegression()
    # Run a linear regression to get beta of Spy Pct Change on Security's (AAPL's) pct change
    regr.fit(X, y)
    # Gets Beta
    beta = regr.coef_[0][0]
    beta = "%.2f" % round(beta, 2)
    # Plot the line of best fit 
    plt.style.use('fivethirtyeight')
    plt.figure(figsize=(10,5))
    # Predictions for stock pct change given spy pct change
    y_pred = regr.predict(X)
    plt.scatter(X, y)
    plt.plot(X, y_pred, color="red")
    plt.xlabel("Market Return")
    plt.ylabel(ticker+" Return")
    plt.title("BETA of " + ticker + ": " + beta)
    image_url = './static/'+ticker+'.png'
    plt.savefig(image_url,bbox_inches="tight")

    return beta

def get_beta(ticker, start, end):
    spy_df = get_stock_data("SPY", start, end)
    stock_df = get_stock_data(ticker, start, end)
    preprocessed_df = preprocess_data(spy_df, stock_df, ticker)
    return get_regression_and_plot(preprocessed_df, ticker)

    # def showGraph(self):
    #     s = pd.Series([1, 2, 3])
    #     fig, ax = plt.subplots()
    #     s.plot.bar()
    #     fig.savefig('my_plot.png')
    #     return render_template('plot.')

if __name__ == '__main__':
    app.run(debug=True)
